FROM golang

ADD . /go/mumla

RUN cd mumla && go build && mv mumla /

CMD ["/mumla", "-host", "murmur"]
