# Mumble Positional Audio Placement Bot

Simple(ish) GRPC Mumble bot for automatic placment when using a positional audio
plugin.

```
usage: ./mumla [flags]
  -chan string
    	channel to manage (default "Root/Games")
  -host string
    	host to connect to (default "localhost")
  -port int
    	port for grpc connection (default 50051)
```

Users who link a game with a positional audio plugin will be automatically
moved to a channel under the one specified in the `-chan` argument. For
example, if you linked Valheim and joined a world called "Valhalla," you
would automatically be moved to the `Root/Games/Valheim/Valhalla` channel.

Users who are not linked to a game will be placed in the base managed
channel. So if you were linked as in the previous example, and then exit
Valhiem, you'd automatically be moved to `Root/Games`.

Channels that are not nested under the `-chan` argument will not be affected.