module gitlab.com/jrobsonchase/mumla

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/golang/protobuf v1.4.3
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210228012217-479acdf4ea46 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20210226172003-ab064af71705 // indirect
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
	layeh.com/gumble v0.0.0-20200818122324-146f9205029b
)
