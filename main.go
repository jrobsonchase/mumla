package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/jrobsonchase/mumla/MurmurRPC"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"
	_ "layeh.com/gumble/opus"
)

func main() {
	ctx := context.Background()
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: %s [flags]\n", os.Args[0])
		flag.PrintDefaults()
	}
	channel := flag.String("chan", "Root/Games", "channel to manage")
	host := flag.String("host", "localhost", "host to connect to")
	grpcPort := flag.Int("port", 50051, "port for grpc connection")
	flag.Parse()

	grpcConn, err := grpc.Dial(net.JoinHostPort(*host, fmt.Sprint(*grpcPort)), grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer grpcConn.Close()

	server := &MurmurRPC.Server{
		Id: proto.Uint32(1),
	}

	murmur := newClient(MurmurRPC.NewV1Client(grpcConn), server)

	rootID := int64(-1)

	if channels, err := murmur.channels(ctx); err != nil {
		log.Fatalf("error getting channel list: %s", err.Error())
	} else {
		channelPath := strings.Split(*channel, "/")
		for _, ch := range channels {
			if murmur.matchesPath(ch, channelPath) {
				rootID = int64(*ch.Id)
				log.Printf("Found root channel %d with path %v", rootID, *channel)
				murmur.setRoot(ch, channelPath)
				break
			}
		}
	}

	if rootID == -1 {
		log.Fatalf("failed to find root channel")
	}

	// Get the initial state of the connected users that are in the same channel
	// as the debug user
	if users, err := murmur.users(ctx); err != nil {
		log.Fatalf("error getting user list: %v", err)
	} else {
		for _, u := range users {
			if err := murmur.rectifyUser(ctx, u); err != nil {
				log.Printf("error moving user %s: %v", u.GetName(), err)
			}
		}
	}

	stream, err := murmur.ServerEvents(ctx, server)
	if err != nil {
		log.Fatalf("error getting event stream: %v", err)
	}
	for {
		event, err := stream.Recv()
		if err != nil {
			log.Fatalf("event stream ended with error: %v", err)
		}

		ty := event.GetType()
		switch ty {
		case MurmurRPC.Server_Event_UserStateChanged:
			err = murmur.rectifyUser(ctx, event.User)
		case MurmurRPC.Server_Event_UserConnected:
			err = murmur.rectifyUser(ctx, event.User)
		case MurmurRPC.Server_Event_ChannelCreated:
			murmur.addChannel(event.Channel)
		case MurmurRPC.Server_Event_ChannelStateChanged:
			murmur.addChannel(event.Channel)
		case MurmurRPC.Server_Event_ChannelRemoved:
			if event.Channel.GetId() == murmur.rootChannel.GetId() {
				log.Fatal("Our root channel was removed, bailing out!")
			}
			murmur.removeChannel(event.Channel.GetId())
		case MurmurRPC.Server_Event_UserDisconnected:
			murmur.removeUser(event.User.GetName())
			murmur.cleanupChannels(ctx)
		}

		if err != nil {
			log.Printf("error relocating user: %s", err)
		}
	}
}

type client struct {
	MurmurRPC.V1Client
	server       *MurmurRPC.Server
	rootChannel  *MurmurRPC.Channel
	rootPath     []string
	userCache    map[string]*MurmurRPC.User
	channelCache map[uint32]*MurmurRPC.Channel
	membership   map[uint32]uint32
}

func newClient(grpc MurmurRPC.V1Client, server *MurmurRPC.Server) client {
	return client{
		V1Client:     grpc,
		server:       server,
		userCache:    map[string]*MurmurRPC.User{},
		channelCache: map[uint32]*MurmurRPC.Channel{},
		membership:   map[uint32]uint32{},
	}
}

func (cl *client) removeChannel(id uint32) {
	delete(cl.channelCache, id)
	delete(cl.membership, id)
}

func (cl *client) removeUser(name string) {
	u := cl.userCache[name]
	delete(cl.userCache, name)
	cl.membership[u.GetChannel().GetId()]--
}

func (cl *client) addChannel(channel *MurmurRPC.Channel) {
	if _, ok := cl.channelCache[channel.GetId()]; !ok {
		log.Printf("Adding channel %s (id: %d)", channel.GetName(), channel.GetId())
		cl.membership[channel.GetId()] = 0
	}
	cl.channelCache[channel.GetId()] = channel
}

func (cl *client) addUser(user *MurmurRPC.User) {
	if prev, ok := cl.userCache[user.GetName()]; !ok {
		log.Printf("Adding user %s (id: %d)", user.GetName(), user.GetId())
	} else {
		log.Printf("User %s changed. channel: %s, context: %s",
			user.GetName(),
			strings.Join(cl.getPath(user.GetChannel()), "/"),
			strconv.Quote(string(user.GetPluginContext())),
		)

		cl.membership[prev.GetChannel().GetId()]--
	}
	cl.membership[user.GetChannel().GetId()]++
	copied := new(MurmurRPC.User)
	//nolint:govet // we're not going to use the lock, so this is fine
	*copied = *user
	cl.userCache[user.GetName()] = copied
}

func (cl *client) users(ctx context.Context) ([]*MurmurRPC.User, error) {
	if users, err := cl.UserQuery(ctx, &MurmurRPC.User_Query{
		Server: cl.server,
	}); err != nil {
		return nil, err
	} else {
		cl.userCache = map[string]*MurmurRPC.User{}
		return users.Users, nil
	}
}

func (cl *client) channels(ctx context.Context) ([]*MurmurRPC.Channel, error) {
	if channels, err := cl.ChannelQuery(ctx, &MurmurRPC.Channel_Query{
		Server: cl.server,
	}); err != nil {
		return nil, err
	} else {
		cl.channelCache = map[uint32]*MurmurRPC.Channel{}
		for _, ch := range channels.Channels {
			cl.addChannel(ch)
		}
		return channels.Channels, nil
	}
}

func (cl *client) rectifyUser(ctx context.Context, user *MurmurRPC.User) error {
	if user == nil {
		return nil
	}

	channelChanged := true
	contextChanged := true
	if prev, ok := cl.userCache[user.GetName()]; ok {
		channelChanged = user.GetChannel().GetId() != prev.GetChannel().GetId()
		contextChanged = !reflect.DeepEqual(user.GetPluginContext(), prev.GetPluginContext())
	}

	if !(channelChanged || contextChanged) {
		return nil
	}

	cl.addUser(user)

	if user.GetName() == "SuperUser" {
		log.Println("... but they're the SuperUser.")
		return nil
	}

	if user.Id != nil {
		var err error
		var acls *MurmurRPC.ACL_List
		if acls, err = cl.ACLGet(ctx, cl.rootChannel); err != nil {
			return fmt.Errorf("error getting acls: %w", err)
		}
		for _, group := range acls.Groups {
			if group.GetName() == "admin" {
				for _, dbUser := range group.GetUsers() {
					if dbUser.GetId() == user.GetId() {
						log.Print("... but they're an admin, so leaving them be.")
						return nil
					}
				}
			}
		}
	}

	if len(user.PluginContext) > 0 {
		path := append(cl.rootPath, strings.Split(string(user.PluginContext), "\x00")...)

		if !cl.matchesPath(cl.fullInfo(user.GetChannel()), path) {
			return cl.relocateUser(ctx, user, path)
		}
		return nil
	} else {
		return cl.relocateUser(ctx, user, []string{})
	}
}

func (cl *client) relocateUser(ctx context.Context, user *MurmurRPC.User, path []string) error {
	var err error
	var channel, userChannel *MurmurRPC.Channel
	userChannel = cl.fullInfo(user.GetChannel())
	if len(path) > 0 {
		channel = cl.findChannel(path)

		if channel == nil {
			if channel, err = cl.createChannel(ctx, path); err != nil {
				return err
			}
		}
	} else {
		if cl.getDepth(cl.rootChannel, userChannel) > 0 {
			channel = cl.rootChannel
		}
	}

	if channel == nil {
		return nil
	}

	user.Channel = channel

	log.Printf("Moving %s to %s", user.GetName(), strings.Join(cl.getPath(channel), "/"))

	u, err := cl.UserUpdate(ctx, user)
	cl.addUser(u)
	cl.cleanupChannels(ctx)
	return err
}

// Get the depth from base to maybeChild. returns -1 if they're not nested
func (cl *client) getDepth(base, maybeChild *MurmurRPC.Channel) int {
	if base == nil || maybeChild == nil {
		return -1
	}
	depth := 0
	for maybeChild != nil {
		if maybeChild.GetId() == base.GetId() {
			return depth
		}
		depth++
		maybeChild = cl.fullInfo(maybeChild.GetParent())
	}
	return -1
}

func (cl *client) fullInfo(ch *MurmurRPC.Channel) *MurmurRPC.Channel {
	if ch == nil {
		return ch
	}

	return cl.channelCache[ch.GetId()]
}

// walk the channels to get the full path
func (cl *client) getPath(ch *MurmurRPC.Channel) []string {
	ch = cl.fullInfo(ch)
	revPath := []string{}
	if ch == nil {
		return revPath
	}

	for ch != nil {
		revPath = append(revPath, ch.GetName())
		ch = cl.fullInfo(ch.GetParent())
	}

	path := make([]string, 0, len(revPath))

	for i := len(revPath) - 1; i >= 0; i-- {
		path = append(path, revPath[i])
	}

	return path
}

func (cl *client) findChannel(path []string) *MurmurRPC.Channel {
	for _, ch := range cl.channelCache {
		if cl.matchesPath(ch, path) {
			return ch
		}
	}

	return nil
}

func (cl *client) createChannel(ctx context.Context, path []string) (*MurmurRPC.Channel, error) {
	var i int
	var last *MurmurRPC.Channel
	for i = 1; i < len(path); i++ {
		if ch := cl.findChannel(path[:i]); ch != nil {
			last = ch
		} else {
			break
		}
	}

	for i--; i < len(path); i++ {
		if ch, err := cl.ChannelAdd(ctx, &MurmurRPC.Channel{
			Server: cl.server,
			Parent: last,
			Name:   &path[i],
		}); err != nil {
			return nil, err
		} else {
			cl.addChannel(ch)
			last = ch
		}
	}

	return last, nil
}

func (cl *client) matchesPath(channel *MurmurRPC.Channel, path []string) bool {
	channelPath := cl.getPath(channel)

	if len(channelPath) != len(path) {
		return false
	}

	for i := 0; i < len(channelPath); i++ {
		if path[i] != channelPath[i] {
			return false
		}
	}

	return true
}

func (cl *client) setRoot(ch *MurmurRPC.Channel, path []string) {
	cl.rootChannel = ch
	cl.rootPath = path
}

func (cl *client) cleanupChannels(ctx context.Context) {
	for id, count := range cl.membership {
		if cl.getDepth(cl.rootChannel, cl.fullInfo(&MurmurRPC.Channel{
			Id: &id,
		})) != 2 {
			continue
		}

		if count == 0 {
			log.Printf("Cleaning up empty channel %d", id)
			if _, err := cl.ChannelRemove(ctx, &MurmurRPC.Channel{
				Id:     &id,
				Server: cl.server,
			}); err != nil {
				log.Printf("Error removing empty channel: %s", err.Error())
				continue
			}
			cl.removeChannel(id)
		}
	}
}
